const remote = require('electron').remote;
var mwindow = remote.getCurrentWindow();

window.eval = global.eval = function() {
	throw new Error('eval() has been disabled.');
}

$(document).ready(function(){
	document.getElementById("handle-close").addEventListener("click", function(){
		mwindow.close();
	});

	document.getElementById("handle-minimize").addEventListener("click", function(){
		mwindow.minimize();
	});

	document.getElementById("handle-maximize").addEventListener("click", function(){
		if(mwindow.isMaximized())
			mwindow.unmaximize();
		else
			mwindow.maximize();
	});
});


